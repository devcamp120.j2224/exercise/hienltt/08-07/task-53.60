package com.devcamp.javabasic;

public interface ISumable {
	String  getSum();
}
